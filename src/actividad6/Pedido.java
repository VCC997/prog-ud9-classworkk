package actividad6;

public class Pedido {

    private int id;
    private Producto productos;
    private String fechaPedido;
    private String nombreCliente;
    private boolean mesaServida;

    public Pedido(int id, Producto productos, String fechaPedido, String cliente, boolean mesaServida) {

        this.id = id;
        this.productos=productos;
        this.fechaPedido = fechaPedido;
        this.nombreCliente = cliente;
        this.mesaServida = mesaServida;
    }

    @Override
    public String toString() {
        return "id=" + id + ", productos=" + productos +
                ", fechaPedido='" + fechaPedido + '\'' +
                ", nombreCliente='" + nombreCliente + '\'' +
                ", mesaServida=" + mesaServida;
    }
}
