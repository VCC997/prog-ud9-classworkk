package actividad8;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Diccionario {

    public static void main(String[] args) {

        System.out.println(isCorrectWord("esp", "ing"));

    }

    public static String isCorrectWord(String esp, String ing) {

        HashMap<String, String> españolIngles = new HashMap<>();

        españolIngles.put("Gato", "Cat");
        españolIngles.put("Perro", "Dog");
        españolIngles.put("Day", "Día");
        españolIngles.put("Nigth", "Noche");
        españolIngles.put("Afternoon", "Tarde");
        españolIngles.put("Time", "Tiempo");
        españolIngles.put("Hourglass", "Reloj de Arena");
        españolIngles.put("Sand", "Arena");
        españolIngles.put("Fish", "Pez");
        españolIngles.put("Bird", "Pajaro");
        españolIngles.put("Car", "Coche");
        españolIngles.put("Bus", "Autobus");
        españolIngles.put("Both", "Ambos");
        españolIngles.put("Begin", "Empezar");
        españolIngles.put("Red", "Rojo");
        españolIngles.put("Yellow", "Amarillo");
        españolIngles.put("Brown", "Marron");
        españolIngles.put("Purple", "Violeta");
        españolIngles.put("Blue", "Azul");
        españolIngles.put("Green", "Verde");

        for (Map.Entry actual : españolIngles.entrySet()) {

            Scanner scanner = new Scanner(System.in);
            String palabra = "";

            System.out.println("Escribe la palabra que quieras traducir:");
            palabra=scanner.nextLine();

            if (españolIngles.containsKey(palabra)) {
                System.out.println("La palabra:"+actual.getKey().equals(palabra)+" "+"en inglés es...");
                System.out.println(actual.getValue());

            }
        }

        return ing;
    }
}
