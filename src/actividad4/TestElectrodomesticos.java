package actividad4;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestElectrodomesticos {

    public static void main(String [] args){

        ArrayList<Electrodomestico> electrodomesticos = new ArrayList<>();

        electrodomesticos.add(new Lavadora (200, "Balay", "Medio",
                        Electrodomestico.Color.AZUL, Electrodomestico.Consumo.A ));

        electrodomesticos.add(new Television(350, "Sony", "Grande",
                Electrodomestico.Color.NEGRO, Electrodomestico.Consumo.B));

        electrodomesticos.add(new Lavadora(300, "Balay", "Pequeño",
                Electrodomestico.Color.BLANCO, Electrodomestico.Consumo.B));

        electrodomesticos.add(new Television(400, "Samsung", "Grande",
                Electrodomestico.Color.BLANCO, Electrodomestico.Consumo.A));

        electrodomesticos.add(new Lavadora(250, "Made China", "Medio",
                Electrodomestico.Color.GRIS, Electrodomestico.Consumo.D));

        for (int i = 0; i < electrodomesticos.size(); i++) {

            electrodomesticos.get(i);
        }
    }

    public void showCosteTotal(List<Electrodomestico> electrodomesticos){
        double totalTele=0;
        double totalLavadora=0;

        Iterator iterator = electrodomesticos.iterator();

        while (iterator.hasNext()){
            Electrodomestico actual = (Electrodomestico) iterator.next();
            if (actual instanceof Television){
                totalTele +=actual.precioVenta();
            } else {
                totalLavadora += actual.precioVenta();
            }
        }

        System.out.printf("Total teles: %.2f \n"+totalTele);
        System.out.printf("Total teles: %.2f \n"+totalLavadora);
        System.out.printf("Total precio: %.2f \n"+totalTele+totalLavadora);
    }
}
