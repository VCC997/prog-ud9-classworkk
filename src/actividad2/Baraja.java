package actividad2;

import java.util.ArrayList;
import java.util.Collections;

public class Baraja {

    public static String[] palos={"diamantes", "tréboles", "corazones", "espadas"};
    private ArrayList<Carta> cartas;

    public Baraja(){

        cartas = new ArrayList<Carta>();
        for(String palo: palos)
            for(int i=1;i<=13;i++)
                cartas.add(new Carta(i, palo));

    }

    public void barajar(){
        Collections.shuffle(cartas);
    }

    public ArrayList<Carta> sacar(int n){
        ArrayList<Carta> rv = new ArrayList<Carta>();
        for(int i=0;i<n;i++)
            rv.add(cartas.remove(0));
        return rv;
    }

    public Carta sacar(){
        return cartas.remove(0);
    }

    public ArrayList<Carta> getCartas(){
        return this.cartas;
    }

    public void mostrarBaraja() {
        for(int i = 0; i < cartas.size(); i++)
        {
            Carta carta = cartas.get(i);
            System.out.println(carta.toString());
        }
    }

    public int tamañobaraja() {
        return cartas.size();
    }

    public String toString(){
        return cartas.toString();
    }
}
