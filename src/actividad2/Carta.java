package actividad2;

public class Carta {

    private int num;
    private String palo;

    /*enum Palos{

        OROS{
            @Override
            public String toString() {
                return "0";
            }
        },
        COPAS{
            @Override
            public String toString() {
                return "P";
            }
        },
        ESPADAS{
            @Override
            public String toString() {
                return "T";
            }
        },
        BASTOS{
            @Override
            public String toString() {
                return "|";
            }
        };
    }*/

    public Carta(int num, String palo)
    {
        this.num = num;
        this.palo = palo;

    }

    @Override
    public String toString(){
        return "Numero de Carta:"+num+"\t"+"Palo:"+palo;
    }
}
